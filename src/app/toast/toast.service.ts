import {Injectable, Injector, Inject, PLATFORM_ID} from '@angular/core';
import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';

import { ToastComponent } from './toast.component';
import { ToastData, TOAST_CONFIG_TOKEN, ToastConfig } from './toast-config';
import { ToastRef } from './toast-ref';
import { isPlatformBrowser } from '@angular/common';

@Injectable()
export class ToastService {
    private lastToast: ToastRef;

    constructor(
        private overlay: Overlay,
        private parentInjector: Injector,
        @Inject(TOAST_CONFIG_TOKEN) private toastConfig: ToastConfig,
        @Inject(PLATFORM_ID) private platform: any
    ) { }

    show(data: ToastData) {
        if (isPlatformBrowser(this.platform)) {
            const positionStrategy = this.getPositionStrategy();
            const overlayRef = this.overlay.create({ positionStrategy });

            const toastRef = new ToastRef(overlayRef);
            this.lastToast = toastRef;

            const injector = this.getInjector(data, toastRef, this.parentInjector);
            const toastPortal = new ComponentPortal(ToastComponent, null, injector);

            overlayRef.attach(toastPortal);

            return toastRef;
        }
    }

    getPositionStrategy() {
        return this.overlay.position()
            .global()
            .top(this.getPosition())
            .right(this.toastConfig.position.right + 'px');
    }

    getPosition() {
        const lastToastIsVisible = this.lastToast && this.lastToast.isVisible();
        const position = lastToastIsVisible
            ? this.lastToast.getPosition().top
            : this.toastConfig.position.bottom;

        return position + 'px';
    }

    getInjector(data: ToastData, toastRef: ToastRef, parentInjector: Injector) {
        const tokens = new WeakMap();

        tokens.set(ToastData, data);
        tokens.set(ToastRef, toastRef);

        return new PortalInjector(parentInjector, tokens);
    }
}
