import { NgModule, ModuleWithProviders } from '@angular/core';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatIconModule } from '@angular/material/icon';

import { ToastComponent } from './toast.component';
import { defaultToastConfig, TOAST_CONFIG_TOKEN } from './toast-config';
import { ToastService } from './toast.service';

@NgModule({
    imports: [OverlayModule, MatIconModule],
    declarations: [ToastComponent],
    providers: [
      ToastService,
      {
        provide: TOAST_CONFIG_TOKEN,
        useValue: { ...defaultToastConfig },
      },
    ],
    entryComponents: [ToastComponent]
})
export class ToastModule {}
