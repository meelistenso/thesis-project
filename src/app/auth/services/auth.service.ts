import { Injectable } from '@angular/core';
import { BehaviorSubject ,  ReplaySubject } from 'rxjs';

import { distinctUntilChanged } from 'rxjs/operators';
import { JwtService, UserApiService } from '../../api/services';
import { IUser } from '../../models';
import { AuthApiService } from '../../api/services/data';
import { LoginDto, RegisterDto } from '../../api/models/dto/auth';
import {RequestResponse} from '../../api/models/request-response';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {
  private currentUserSubject = new BehaviorSubject<IUser>({} as IUser);
  public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

  private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor(
    private readonly authApiService: AuthApiService,
    private readonly userApiService: UserApiService,
    private readonly jwtService: JwtService,
    private readonly router: Router,
  ) {}

  async loadUser(): Promise<IUser> {
    return await this.userApiService.getUser();
  }

  // Verify JWT in localstorage with server & load user's info.
  // This runs once on application startup.
  async populate() {
    // If JWT detected, attempt to get & store user's info

    if (this.jwtService.getToken()) {
      const data = this.jwtService.getToken();
      if (data) {
        // Save JWT sent from server in localstorage
        this.jwtService.saveToken(data);
        const user = await this.loadUser();
        if (user) {
          this.setAuth(data, user);
          return;
        } else {
          this.purgeAuth();
        }
      } else {
        this.purgeAuth();
      }
      // this.authService.('/user')
      // .subscribe(
      //   data => this.setAuth(data.user),
      //   err => this.purgeAuth()
      // );
    } else {
      // Remove any potential remnants of previous auth states
      this.purgeAuth();
    }
    this.router.navigate(['/login']);
  }

  setAuth(token: string, user: IUser) {
    // Save JWT sent from server in localstorage
    this.jwtService.saveToken(token);
    // Set current user data into observable
    this.currentUserSubject.next({...user});
    // Set isAuthenticated to true
    this.isAuthenticatedSubject.next(true);
    console.log(`Set Auth: ${user}`);
  }

  purgeAuth() {
    // Remove JWT from localstorage
    this.jwtService.destroyToken();
    // Set current user to an empty object
    this.currentUserSubject.next({} as IUser);
    // Set auth status to false
    this.isAuthenticatedSubject.next(false);
    console.log(`Purge Auth`);
  }

  async attemptAuth(dto: LoginDto): Promise<any> {
    const data = await this.authApiService.login(dto);
    console.log(data);
    if (data) {
      this.setAuth(data.access_token, data.User);
    }
    return data;
  }

  async register(dto: RegisterDto): Promise<any> {
    const data = await this.authApiService.registration(dto);
    console.log(data);
    if (data) {
      this.setAuth(data.access_token, data.User);
    }
    return data;
  }

  async logout(): Promise<RequestResponse<undefined>> {
    return await this.authApiService.logout();
  }

  getCurrentUser(): IUser {
    return this.currentUserSubject.value;
  }

  // Update the user on the server (email, pass, etc)
  update(user) {
    // TODO
  }

}
