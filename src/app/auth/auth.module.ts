import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NoAuthGuard } from './guargs';
import { AuthService } from './services';
import { ShowAuthedDirective } from './directives';
import { ApiModule } from '../api/api.module';
import {AuthGuard} from './guargs/auth-guard.service';

@NgModule({
  declarations: [
    ShowAuthedDirective
  ],
  imports: [
    CommonModule,
    ApiModule,
  ],
  providers: [
    AuthService,
    NoAuthGuard,
    AuthGuard,
    ShowAuthedDirective,
  ]
})
export class AuthModule { }
