﻿import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class HelperService {
    addContentTypeHeader = false;

    private _loading = new BehaviorSubject<boolean>(false);

    get loading(): Observable<boolean> {
        return this._loading.asObservable();
    }

    startLoader() {
        this._loading.next(false);
    }

    stopLoader() {
        this._loading.next(false);
    }
}
