import { Injectable } from '@angular/core';
import { LocalStorageService } from '../../local-storage/local-storage.service';

@Injectable()
export class JwtService {

  constructor(private readonly localStorageService: LocalStorageService) {}

  getToken(): string {
    const token = this.localStorageService.getValue<string>('token', 'jwt');
    if (token) {
      return token;
    }
  }

  getTokenData(): any {
    const token = this.localStorageService.getValue<string>('token', 'jwt');
    try {
      if (token) {
        return JSON.parse(atob(token.split('.')[1]));
      }
    } catch (e) {
      console.error(e);
    }
  }

  saveToken(token: string) {
    this.localStorageService.setValue<string>(token, 'token', 'jwt');
  }

  destroyToken() {
    this.localStorageService.removeValue<string>('token', 'jwt');
  }
}
