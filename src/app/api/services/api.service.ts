import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { JwtService } from './jwt.service';

import { RequestResponse } from '../models/request-response';
import { HelperService } from './helper.service';
import { ToastService } from '../../toast';
import { ToastData } from '../../toast/toast-config';

@Injectable()
export class ApiService {
  private SERVER_URL = environment.api_url;

  constructor(
    private http: HttpClient,
    private jwtService: JwtService,
    private helperService: HelperService,
    private toastService: ToastService,
  ) { }

  private errorInterceptor(error: any) {
    console.log('errorInterceptor', error);
    const data = new ToastData();
    data.text = error.message;
    data.title = error.statusText;
    data.type = 'warning';
    this.toastService.show(data);
  }

  private statusInterceptor<T>(response: RequestResponse<T>): T {
    console.log('statusInterceptor', response);
    if (response.Success === true) {
      return response.Data;
    }
    throw new Error(response.Message);
  }

  async get<T>(path: string, params: HttpParams = new HttpParams()): Promise<T> {
    this.helperService.startLoader();
    return await this.http.get<RequestResponse<T>>(`${this.SERVER_URL}/${path}`, { params })
      .toPromise()
      .then((res) => {
        this.helperService.stopLoader();
        return res;
      })
      .then(res => this.statusInterceptor<T>(res))
      .catch(error => {
        this.errorInterceptor(error);
        return null;
      });
  }

  async put<T>(path: string, body: any = {}): Promise<T> {
    this.helperService.startLoader();
    return await this.http.put<RequestResponse<T>>(`${this.SERVER_URL}/${path}`, body )
      .toPromise()
      .then((res) => {
        this.helperService.stopLoader();
        return res;
      })
      .then(res => this.statusInterceptor<T>(res))
      .catch(error => {
        this.errorInterceptor(error);
        return null;
      });
  }

  async post<T>(path: string, body: any = {}): Promise<T> {
    this.helperService.startLoader();
    return await this.http.post<RequestResponse<T>>(`${this.SERVER_URL}/${path}`, body)
      .toPromise()
      .then((res) => {
        this.helperService.stopLoader();
        return res;
      })
      .then(res => this.statusInterceptor<T>(res))
      .catch(error => {
        this.errorInterceptor(error);
        return null;
      });
  }

  async delete<T>(path): Promise<T> {
    this.helperService.startLoader();
    return await this.http.delete<RequestResponse<T>>(`${this.SERVER_URL}/${path}`)
      .toPromise()
      .then((res) => {
        this.helperService.stopLoader();
        return res;
      })
      .then(res => this.statusInterceptor<T>(res))
      .catch(error => {
        this.errorInterceptor(error);
        return null;
      });
  }
}
