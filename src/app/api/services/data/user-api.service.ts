import { Injectable } from '@angular/core';
import { GetUserRo } from '../../models/ro/user';
import { ApiService } from '../api.service';
import { GetUsersRo } from '../../models/ro/user';
import {GetUserHistoryRo} from '../../models/ro/user/get-user-history.ro';

@Injectable()
export class UserApiService {

  private DATA_ROOT = 'User';

  constructor(private readonly apiService: ApiService) {}

  async getUser(): Promise<GetUserRo> {
    return this.apiService.get<GetUserRo>(`${this.DATA_ROOT}/GetUser`);
  }

  async getAllUsers(startIndex: number, count: number): Promise<GetUsersRo> {
    return this.apiService.get<GetUsersRo>(`${this.DATA_ROOT}/GetAllUsers?startIndex=${startIndex}&count=${count}`);
  }

  async getHistory(startIndex: number, count: number): Promise<GetUserHistoryRo> {
    return this.apiService.get<GetUserHistoryRo>(`${this.DATA_ROOT}/GetUserHistoryTest?startIndex=${startIndex}&count=${count}`);
  }

}
