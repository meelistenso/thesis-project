import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import {AddQuestionDto, AddTestDto, CheckQuestionDto, EditTestDto} from '../../models/dto/test';
import {AddQuestionRo, CheckQuestionRo, GetTestRo, GetTestsRo} from '../../models/ro/test';
import {GetUserHistoryRo} from '../../models/ro/user/get-user-history.ro';

@Injectable()
export class TestApiService {

  private DATA_ROOT = 'Test';

  constructor(private readonly apiService: ApiService) {}

  async addTest(data: AddTestDto): Promise<undefined> {
    return this.apiService.post<undefined>(`${this.DATA_ROOT}/AddTest`, data);
  }

  async editTest(data: EditTestDto): Promise<undefined> {
    return this.apiService.post<undefined>(`${this.DATA_ROOT}/EditTest`, data);
  }

  async getTestById(testId: number): Promise<GetTestRo> {
    return this.apiService.get<GetTestRo>(`${this.DATA_ROOT}/GetTestById?testId=${testId}`);
  }

  async checkQuestions(data: CheckQuestionDto[], testId: number): Promise<CheckQuestionRo> {
    return this.apiService.post<CheckQuestionRo>(`${this.DATA_ROOT}/CheckQuestion?testId=${testId}`, data);
  }

  async addQuestions(data: AddQuestionDto[], testId: string): Promise<AddQuestionRo[]> {
    return this.apiService.post<AddQuestionRo[]>(`${this.DATA_ROOT}/AddQuestion?testId=${testId}`, data);
  }

  async getTestsByTopicId(topicId: number, startIndex: number = 0, count: number = 50): Promise<GetTestsRo> {
    return this.apiService.get<GetTestsRo>(`${this.DATA_ROOT}/GetTestByTopicId?topicId=${topicId}&startIndex=${startIndex}&count=${count}`);
  }

  async getTestsByUserId(startIndex: number = 0, count: number = 50): Promise<GetTestsRo> {
    return this.apiService.get<GetTestsRo>(`${this.DATA_ROOT}/GetTestByUserId?startIndex=${startIndex}&count=${count}`);
  }

  async getPopularTests(startIndex: number = 0, count: number = 50): Promise<GetTestsRo> {
    return this.apiService.get<GetTestsRo>(`${this.DATA_ROOT}/GetPopularTest?startIndex=${startIndex}&count=${count}`);
  }

  async getAllTests(startIndex: number = 0, count: number = 50): Promise<GetTestsRo> {
    return this.apiService.get<GetTestsRo>(`${this.DATA_ROOT}/GetAllTest?startIndex=${startIndex}&count=${count}`);
  }

}
