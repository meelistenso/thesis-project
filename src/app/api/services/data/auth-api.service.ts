import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { LoginDto, RegisterDto, RefreshDto } from '../../models/dto';
import { LoginRo, RegisterRo, RefreshRo } from '../../models/ro';
import {GetUserRo} from '../../models/ro/user/get-user.ro';
import {RequestResponse} from '../../models/request-response';

@Injectable()
export class AuthApiService {

  private DATA_ROOT = 'Authentication';

  constructor(private readonly apiService: ApiService) {}

  async registration(data: RegisterDto): Promise<RegisterRo> {
    return this.apiService.post<RegisterRo>(`${this.DATA_ROOT}/Registration`, data);
  }

  async login(data: LoginDto): Promise<LoginRo> {
    return this.apiService.post<LoginRo>(`${this.DATA_ROOT}/Login`, data);
  }

  async refresh(data: RefreshDto): Promise<RefreshRo> {
    return this.apiService.post<RefreshRo>(`${this.DATA_ROOT}/Refresh`, data);
  }

  async logout(): Promise<RequestResponse<undefined>> {
    return this.apiService.post(`${this.DATA_ROOT}/Logout`);
  }
}
