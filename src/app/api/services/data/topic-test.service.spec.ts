import { TestBed } from '@angular/core/testing';

import { TopicTestApiService } from './topic-test-api.service';

describe('TopicTestService', () => {
  let service: TopicTestApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopicTestApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
