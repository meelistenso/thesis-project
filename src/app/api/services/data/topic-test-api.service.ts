import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { GetTestTopicsRo} from '../../models/ro/topic-test';
import { RequestResponse } from '../../models/request-response';

@Injectable()
export class TopicTestApiService {
  private DATA_ROOT = 'TopicTest';

  constructor(private readonly apiService: ApiService) {}

  async getAllTestTopics(): Promise<GetTestTopicsRo> {
    return this.apiService.get<GetTestTopicsRo>(`${this.DATA_ROOT}/GetAllTestTopics`);
  }

  async addTopicTest(name: string, parentId: string): Promise<RequestResponse<undefined>> {
    return this.apiService.get<RequestResponse<undefined>>(`${this.DATA_ROOT}/AddTopicTest?name=${name}&parentId=${parentId}`);
  }

  async deleteTopicTest(): Promise<RequestResponse<undefined>> {
    return this.apiService.get<RequestResponse<undefined>>(`${this.DATA_ROOT}/DeleteTopicTest`);
  }

  async searchTopics(value: string): Promise<GetTestTopicsRo> {
    return this.apiService.get<GetTestTopicsRo>(`${this.DATA_ROOT}/Search?search=${value}`);
  }
}
