import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTokenInterceptor } from './interceptors';
import {
  ApiService,
  JwtService,
  AuthApiService,
  TestApiService,
  TopicTestApiService,
  HelperService,
  UserApiService
} from './services';
import { LocalStorageModule } from '../local-storage/local-storage.module';
import { ToastModule } from '../toast';

@NgModule({
  declarations: [],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    ApiService,
    JwtService,
    AuthApiService,
    TestApiService,
    TopicTestApiService,
    HelperService,
    UserApiService,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    LocalStorageModule,
    ToastModule
  ]
})
export class ApiModule { }
