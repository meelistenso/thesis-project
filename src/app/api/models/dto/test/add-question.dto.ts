import { IVersion } from '../../../../models';

export class AddQuestionDto {
  Title: string;
  Versions: IVersion[];
}
