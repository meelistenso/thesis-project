export class EditTestDto {
  Id?: number;
  Questions?: [
    {
      Id: number,
      Title: string,
      Weight: number,
      Versions: [
        {
          Id: number,
          Title: string,
          IsTrue: true
        }
      ],
      CountMiss: number,
      QuestionType: number
    }
  ];
  Title?: string;
  Description?: string;
  UserId?: number;
  TopicTestId?: number;
}
