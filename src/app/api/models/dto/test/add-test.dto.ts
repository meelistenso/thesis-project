import { IQuestion, ITestData } from '../../../../models';

export class AddTestDto {
  Test: {
    Title: string;
    Description: string;
    TopicTestId: number;
  };
  QuestionRequestModels?: IQuestion[];
}
