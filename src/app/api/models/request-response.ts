export interface RequestResponse<T> {
  Data: T;
  Success: boolean;
  Message: string | null;
}
