export interface DataList<T> {
  List: T[];
  Count: number;
}
