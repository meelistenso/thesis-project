import { DataList } from '../../data-list';
import { GetUserRo } from './get-user.ro';

export interface GetUsersRo extends DataList<GetUserRo>{

}
