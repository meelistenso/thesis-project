export interface GetUserRo {
  Id: number;
  FirstName: string;
  LastName: string;
  Email: string;
}
