import { DataList } from '../../data-list';

export interface GetUserHistoryRo extends DataList<{
  NumberOfTrueTests: number;
  Mark: number;
  FinishTestDate: string;
  TestId: number;
}> { }
