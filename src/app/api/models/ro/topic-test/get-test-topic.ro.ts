export interface GetTestTopicRo {
  Id: number;
  Name: string;
  ParentTopicTestId: number;
  TopicTestModels?: GetTestTopicRo[];
}
