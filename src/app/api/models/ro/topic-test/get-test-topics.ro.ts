import { DataList } from '../../data-list';
import { GetTestTopicRo } from './get-test-topic.ro';

export interface GetTestTopicsRo extends DataList<GetTestTopicRo>{ }
