import { GetUserRo } from '../user';

export interface LoginRo {
  User: GetUserRo;
  access_token: string;
}
