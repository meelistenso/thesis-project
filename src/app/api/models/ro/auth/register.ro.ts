import { GetUserRo } from '../user';

export interface RegisterRo {
  User: GetUserRo;
  access_token: string;
}
