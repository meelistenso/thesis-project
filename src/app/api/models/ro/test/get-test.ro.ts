import { QuestionType } from '../../../../models';

export interface GetTestRo {
  Id: number;
  Questions: [
    {
      Id: number;
      Title: string;
      Weight: number;
      QuestionType: QuestionType;
      Versions: [
        {
          Id: number;
          Title: string;
          IsTrue: boolean;
        }
      ],
      CountMiss: number;
    }
  ];
  Title: string;
  Description: string;
  UserId: number;
  TopicTestId: number;
}
