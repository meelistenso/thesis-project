import { DataList } from '../../data-list';
import { ITestData } from '../../../../models/data';

export interface GetTestsRo extends DataList<ITestData>{ }
