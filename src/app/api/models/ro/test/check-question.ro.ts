export interface CheckQuestionRo {
  Mark: number;
  Answers: {
    QuestionId: number;
    Weight: number;
    IsTrueAnswer: number;
  }[];
}
