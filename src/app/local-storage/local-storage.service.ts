﻿import { Injectable } from '@angular/core';

const MAX_STORE_TIME = 365 * 24 * 3600 * 1000;

@Injectable()
export class LocalStorageService {
    getValue<T>(key: string, cacheKey?: string): T | false {
        const localKey = cacheKey ? key + cacheKey : key;
        try {
            const data = localStorage.getItem(localKey);
            const timestamp = this.getTimestamp(localKey);
            const maxStoreTime = this.getMaxStoreTime(localKey);
            if ((Date.now() - timestamp) > maxStoreTime) {
                return false;
            }
            return JSON.parse(data);
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    setValue<T>(value: T, key: string, cacheKey?: string, maxStoreTime?: number)
    {
        const localKey = cacheKey ? key + cacheKey : key;
        this.setTimestamp(localKey);
        this.setMaxStoreTime(localKey, maxStoreTime || MAX_STORE_TIME);
        const dataString = JSON.stringify(value);
        console.log(`setValue: ${localKey} <| ${dataString}`);
        localStorage.setItem(localKey, dataString);
    }

    removeValue<T>(key: string, cacheKey?: string): boolean {
        const localKey = cacheKey ? key + cacheKey : key;
        try {
            const data = localStorage.removeItem(localKey);
            return true;
        } catch (e) {
            return false;
        }
    }

    private getTimestamp(key: string): number {
        return +localStorage.getItem(`${key}-timestamp`);
    }

    private getMaxStoreTime(key: string): number {
        return +localStorage.getItem(`${key}-maxStoreTime`);
    }

    private setTimestamp(key: string) {
        const timestamp = Date.now();
        localStorage.setItem(`${key}-timestamp`, timestamp.toString());
    }

    private setMaxStoreTime(key: string, timestamp: number) {
        localStorage.setItem(`${key}-maxStoreTime`, timestamp.toString());
    }
}
