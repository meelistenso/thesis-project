import { IVersion } from './version.interface';
import { QuestionType } from './question-type.enum';

export interface IQuestion {
  Id: number;
  Title: string;
  Versions: IVersion[];
  Weight: number;
  QuestionType: QuestionType;
}
