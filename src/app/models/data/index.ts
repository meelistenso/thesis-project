export { IVersion } from './version.interface';
export { IQuestion } from './question.interface';
export { ITestData } from './test-data.interface';
export { IUser } from './user.interface';
export { QuestionType } from './question-type.enum';
