export interface ITestData {
  Id: number;
  Title: string;
  Description: string;
  TopicTestId: number;
  UserId?: string;
}
