export interface MenuRoute {
    path: string;
    title: string;
    icon?: string;
    submenu?: MenuRoute[];
}

export const MENU_ROUTES: MenuRoute[] = [
    // {
    //     path: 'home',
    //     title: 'Home',
    //     icon: 'home'
    // },
    {
        path: 'dashboard',
        title: 'Dashboard',
        icon: 'table_chart',
        submenu: [{
            path: '/dashboard/history',
            title: 'Testing history'
        }]
    },
    {
        path: 'tests',
        title: 'Tests',
        icon: 'text_fields',
        submenu: [{
            path: '/tests/popular',
            title: 'Popular tests'
        },
        {
          path: '/tests/topic-list',
          title: 'Topics'
        }]
    },
    {
        path: 'admin',
        title: 'Admin panel',
        icon: 'tab',
        submenu: [
            {
                path: '/admin/create-test',
                title: 'Create test'
            },
            {
              path: '/admin/manage-test',
              title: 'Manage tests'
            }
        ]
    }
];
