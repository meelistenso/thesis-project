import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';

import {MatPaginator} from '@angular/material/paginator';
import { from } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { ITestData } from '../../../models/data';
import { Router } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements AfterViewInit {
  displayedColumns: string[] = ['Id', 'Date', 'Mark', 'Topic', 'Name', 'True answers'];
  data: ITestData[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly dashboardService: DashboardService,
    private readonly router: Router
  ) {}

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dashboardService.getTestHistory(this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data.Count;
          return data.List;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return from([]);
        })
      ).subscribe(data => this.data = data);
  }
}
