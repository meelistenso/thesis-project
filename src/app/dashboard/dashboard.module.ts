import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardService } from './services/dashboard.service';
import { SharedModule } from '../shared/shared.module';
import { HistoryComponent } from './components/history/history.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [DashboardComponent, HistoryComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    MatProgressSpinnerModule
  ],
  providers: [
    DashboardService
  ]
})
export class DashboardModule { }
