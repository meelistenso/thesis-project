import { Injectable } from '@angular/core';
import {TestApiService, UserApiService} from '../../api/services/data';

const ITEMS_PER_PAGE = 10;

@Injectable()
export class DashboardService {

  constructor(private readonly userApiService: UserApiService) { }

  getTestHistory(pageIndex: number) {
    return this.userApiService.getHistory(pageIndex * ITEMS_PER_PAGE, ITEMS_PER_PAGE);
  }
}
