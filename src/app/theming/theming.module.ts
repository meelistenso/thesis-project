import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemingService } from './theming.service';

@NgModule({
  declarations: [],
  providers: [
    ThemingService
  ],
  imports: [
    CommonModule
  ]
})
export class ThemingModule { }
