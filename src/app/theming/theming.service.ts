import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { OverlayContainer } from '@angular/cdk/overlay';

import { AppSettings } from '../settings';
import { LocalStorageService } from '../local-storage/local-storage.service';

@Injectable()
export class ThemingService {
  public theme: string;
  public theming$: Observable<string>;
  private themingEmitter: BehaviorSubject<string>;

  constructor(
    private overlayContainer: OverlayContainer,
    private localStorageService: LocalStorageService
  ) {
    this.theme = this.localStorageService.getValue('theme', 'theming') || AppSettings.defaultTheme;

    this.themingEmitter = new BehaviorSubject<string>(this.theme);
    this.theming$ = this.themingEmitter.asObservable();
    this.addOverlayThemeClass(this.theme);
  }

  public set(theme: string) {
    this.theme = theme;
    this.addOverlayThemeClass(theme);
    this.localStorageService.setValue(theme, 'theme', 'theming');
    // Emit the new class
    this.themingEmitter.next(theme);
  }

  public get(): string {
    return this.theme;
  }

  private addOverlayThemeClass(theme: string) {
    // Check if exists a theme
    if (this.overlayContainer.getContainerElement().classList.length > 1) {
      // Get old class theme name to replace for the new theme
      const oldTheme = this.overlayContainer
        .getContainerElement().classList[this.overlayContainer
        .getContainerElement().classList.length - 1];
      this.overlayContainer
        .getContainerElement().classList
        .replace(oldTheme, theme);
    } else {
      // Add the new theme
      this.overlayContainer
        .getContainerElement().classList
        .add(theme);
    }
  }
}
