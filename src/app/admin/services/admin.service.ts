import { Injectable } from '@angular/core';
import {TestApiService, TopicTestApiService} from '../../api/services/data';
import {GetTestRo, GetTestsRo} from '../../api/models/ro/test';

const ITEMS_PER_PAGE = 10;

@Injectable()
export class AdminService {

  constructor(
    private readonly testApiService: TestApiService,
    private readonly topicTestApiService: TopicTestApiService
  ) { }

  getPopularTests(pageIndex: number): Promise<GetTestsRo> {
    return this.testApiService.getAllTests(pageIndex * ITEMS_PER_PAGE, ITEMS_PER_PAGE);
  }

  getAllTests(pageIndex: number): Promise<GetTestsRo> {
    return this.testApiService.getAllTests(pageIndex * ITEMS_PER_PAGE, ITEMS_PER_PAGE);
  }

  deleteTest(id: number) {

  }
}
