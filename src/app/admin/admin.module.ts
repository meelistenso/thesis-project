import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './components/admin/admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared/shared.module';
import { CreateTestComponent } from './components/create-test/create-test.component';
import { ManageTestsComponent } from './components/manage-tests/manage-tests.component';
import { AdminTestListComponent } from './components/admin-test-list/admin-test-list.component';
import { AdminService } from './services/admin.service';
import { TestModule } from '../test/test.module';
import { EditTestComponent } from './components/edit-test/edit-test.component';
import { QuestionsListComponent } from './components/questions-list/questions-list.component';

@NgModule({
  declarations: [
    AdminComponent,
    CreateTestComponent,
    ManageTestsComponent,
    AdminTestListComponent,
    EditTestComponent,
    QuestionsListComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    TestModule,
  ],
  providers: [
    AdminService,
  ]
})
export class AdminModule { }
