import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import {CreateTestComponent} from './components/create-test/create-test.component';
import {ManageTestsComponent} from './components/manage-tests/manage-tests.component';
import { TestListComponent } from '../test/components';
import {AdminTestListComponent} from './components/admin-test-list/admin-test-list.component';
import {EditTestComponent} from './components/edit-test/edit-test.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'manage',
    pathMatch: 'full',
  },
  {
    path: 'manage-test',
    component: AdminTestListComponent,
  },
  {
    path: 'create-test',
    component: CreateTestComponent,
  },
  {
    path: 'edit-test/:id',
    component: EditTestComponent,
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
