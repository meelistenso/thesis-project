import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { IQuestion, ITestData } from '../../../models/data';
import { MatPaginator } from '@angular/material/paginator';
import { AdminService } from '../../services/admin.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss']
})
export class QuestionsListComponent implements AfterViewInit {
  @Input() set questions(questions: IQuestion[]) {

  }
  @Input() isLoadingResults: boolean;

  displayedColumns: string[] = ['Id', 'Title', 'Description', 'Controls'];
  data: ITestData[] = [];

  resultsLength = 0;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly adminService: AdminService,
    private readonly router: Router,
    private readonly dialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef
  ) {}

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.adminService.getAllTests(this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data.Count;
          return data.List;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return from([]);
        })
      ).subscribe(data => this.data = data);
  }

}
