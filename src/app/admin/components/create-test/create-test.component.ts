import { Component, OnInit } from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {Router} from '@angular/router';
import {AuthService} from '../../../auth/services';
import {LoginDto} from '../../../api/models/dto/auth';
import {AddTestDto} from '../../../api/models/dto/test';
import {TestService} from '../../../test/services/test.service';

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.scss']
})
export class CreateTestComponent {

  waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(
    private router: Router,
    private testService: TestService,
  ) {
    this.waiting.next(false);
  }

  async submitForm(data: AddTestDto) {
    await this.testService.addTest(data)
      .then(res => {
        this.waiting.next(false);
        this.router.navigate(['admin', 'manage-test']);
      });
  }
}
