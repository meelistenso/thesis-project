import { Component, OnInit } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {AddTestDto, EditTestDto} from '../../../api/models/dto/test';
import { TestService } from '../../../test/services/test.service';
import { GetTestRo } from '../../../api/models/ro/test';

@Component({
  selector: 'app-edit-test',
  templateUrl: './edit-test.component.html',
  styleUrls: ['./edit-test.component.scss']
})
export class EditTestComponent implements OnInit {

  waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  test: GetTestRo;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private testService: TestService,
  ) {
    this.waiting.next(false);
  }

  ngOnInit() {
      this.waiting.next(true);
      this.route.params.subscribe(async params => {
         const id = params['id'];
         if (id != null) {
             this.test = await this.testService.getTestById(id);
         }
         this.waiting.next(false);
      });
  }

  async submitForm(data: EditTestDto) {
      await this.testService.editTest({ Id: this.test.Id, ...data })
          .then(res => {
            this.waiting.next(false);
            this.router.navigate(['admin', 'manage-test']);
          });
  }
}
