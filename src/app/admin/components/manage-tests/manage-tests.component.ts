import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { ITestData } from '../../../models/data';
import { MatPaginator } from '@angular/material/paginator';
import { DashboardService } from '../../../dashboard/services/dashboard.service';
import { Router } from '@angular/router';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { from } from 'rxjs';

@Component({
  selector: 'app-manage-tests',
  templateUrl: './manage-tests.component.html',
  styleUrls: ['./manage-tests.component.scss']
})
export class ManageTestsComponent implements AfterViewInit {
  displayedColumns: string[] = ['Id', 'Title', 'Description'];
  data: ITestData[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly dashboardService: DashboardService,
    private readonly router: Router
  ) {}

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.dashboardService.getTestHistory(this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data.Count;
          return data.List;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return from([]);
        })
      ).subscribe(data => this.data = data);
  }

  async select(data: ITestData) {
    await this.router.navigate(['/tests', data.Id]);
  }
}
