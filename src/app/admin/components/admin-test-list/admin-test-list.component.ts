import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { AdminService } from '../../services/admin.service';
import { from } from 'rxjs';
import { ITestData } from '../../../models/data';
import { Router } from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {DialogLogoutComponent, DialogRemoveItemComponent} from '../../../shared/components/dialogs';

@Component({
  selector: 'app-admin-test-list',
  templateUrl: './admin-test-list.component.html',
  styleUrls: ['./admin-test-list.component.scss']
})
export class AdminTestListComponent implements AfterViewInit {

  displayedColumns: string[] = ['Id', 'Title', 'Description', 'Controls'];
  data: ITestData[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly adminService: AdminService,
    private readonly router: Router,
    private readonly dialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef
  ) {}

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.adminService.getAllTests(this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data.Count;
          return data.List;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return from([]);
        })
      ).subscribe(data => this.data = data);
  }

  async select(data: ITestData) {
    await this.router.navigate(['/tests', data.Id]);
  }

  async edit(row) {
    await this.router.navigate(['admin', 'edit-test', row.Id]);
  }

  delete(row) {
    const removeDialog = this.dialog.open(DialogRemoveItemComponent, {
      width: '300px',
      data: row
    });

    removeDialog.afterClosed().subscribe(result => {
      if (result) {
        console.log('Delete pressed');
        this.adminService.deleteTest(row.Id);

        this.reload();
      }
    });

  }

  async create() {
    await this.router.navigate(['admin', 'create-test']);
  }

  reload() {
    this.paginator.page.next({
      pageIndex: this.paginator.pageIndex,
      pageSize: this.paginator.pageSize,
      length: this.paginator.length
    });
  }
}
