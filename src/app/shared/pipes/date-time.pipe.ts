import { Pipe, PipeTransform } from '@angular/core';

import * as moment from 'moment';

@Pipe({
  name: 'dateTime'
})
export class DateTimePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): string {
    return moment(value).format('Do MMMM YYYY, h:mm');
  }

}
