import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { LayoutModule } from '@angular/cdk/layout';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import {
  AccountCardComponent, AuthLayoutComponent,
  HeaderComponent,
  LogoutButtonComponent,
  MainLayoutComponent,
  MainNavComponent, MenuComponent, SidenavComponent,
  ToolbarComponent
} from './components/layout';
import { FooterComponent } from './components/layout';
import { ListErrorsComponent } from './components/errors';
import { LoginFormComponent } from './components/forms';
import { RegisterFormComponent } from './components/forms';
import {
  ButtonComponent,
  CheckboxComponent,
  DividerComponent,
  InputComponent,
  LinkComponent,
  LocalLinkComponent,
  OuterLinkComponent,
  PaginatorComponent,
  SectionHeaderComponent,
  StepperComponent,
  SubheaderComponent,
  TextareaComponent
} from './components/UI';

import { ValidationService } from './services';
import { ThemingModule } from '../theming/theming.module';
import { DialogLogoutComponent, DialogRemoveItemComponent } from './components/dialogs';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { SpinnerComponent } from './components';
import { SelectComponent } from './components/UI/select/select.component';
import { MultiselectComponent } from './components/UI/multiselect/multiselect.component';
import { AuthModule } from '../auth/auth.module';

import {
  LoginComponent,
  RegisterComponent
} from './components/auth';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {CreateTestFormComponent} from './components/forms/create-test-form/create-test-form.component';
import { DateTimePipe } from './pipes';
import {EditTestFormComponent} from './components/forms/edit-test-form/edit-test-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    AuthModule,
    // Material
    LayoutModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    // End Material
    PerfectScrollbarModule,
    ThemingModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    ListErrorsComponent,
    LoginFormComponent,
    RegisterFormComponent,
    ButtonComponent,
    CheckboxComponent,
    DividerComponent,
    InputComponent,
    LinkComponent,
    OuterLinkComponent,
    PaginatorComponent,
    SectionHeaderComponent,
    SubheaderComponent,
    TextareaComponent,
    LocalLinkComponent,
    ToolbarComponent,
    LogoutButtonComponent,
    AccountCardComponent,
    MainNavComponent,
    MainLayoutComponent,
    AuthLayoutComponent,
    DialogRemoveItemComponent,
    DialogLogoutComponent,
    BreadcrumbComponent,
    MenuComponent,
    SidenavComponent,
    SpinnerComponent,
    SelectComponent,
    MultiselectComponent,
    LoginComponent,
    RegisterComponent,
    StepperComponent,
    CreateTestFormComponent,
    DateTimePipe,
    EditTestFormComponent
  ],
  providers: [
    ValidationService,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    HeaderComponent,
    FooterComponent,
    ListErrorsComponent,
    ButtonComponent,
    CheckboxComponent,
    DividerComponent,
    InputComponent,
    LinkComponent,
    OuterLinkComponent,
    PaginatorComponent,
    SectionHeaderComponent,
    SubheaderComponent,
    TextareaComponent,
    LoginFormComponent,
    RegisterFormComponent,
    LocalLinkComponent,
    ToolbarComponent,
    LogoutButtonComponent,
    AccountCardComponent,
    MainNavComponent,
    MainLayoutComponent,
    AuthLayoutComponent,
    DialogRemoveItemComponent,
    DialogLogoutComponent,
    BreadcrumbComponent,
    MenuComponent,
    SidenavComponent,
    SpinnerComponent,
    AuthModule,
    /**
     * Material
     */
    // Navigation
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    // Layout
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatExpansionModule,
    MatTabsModule,
    // Form Controls
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    // Buttons & indicators
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    // Popups / modals
    MatDialogModule,
    MatSnackBarModule,
    MatTooltipModule,
    // Data table
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    /* End Material */
    PerfectScrollbarModule,
    ThemingModule,
    LoginComponent,
    RegisterComponent,
    StepperComponent,
    CreateTestFormComponent,
    DateTimePipe,
    EditTestFormComponent
  ]
})
export class SharedModule {}
