import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../auth/services';
import { RegisterDto } from '../../../../api/models/dto/auth';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {
    this.waiting.next(false);
  }

  async submitForm(data: RegisterDto) {
    await this.authService.register(data)
      .then(res => {
        this.router.navigate(['/login']);
        // if (res) {
        //   console.log(res);
        //   console.log('Logged in! Navigating...');
        //   this.router.navigate(['/']);
        // } else {
        //   console.log('Log in failed!');
        // }
        this.waiting.next(false);
      });
  }
}
