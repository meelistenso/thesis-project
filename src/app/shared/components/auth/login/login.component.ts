import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../../auth/services';
import { LoginDto } from '../../../../api/models/dto/auth';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {
    this.waiting.next(false);
  }

  async submitForm(data: LoginDto) {
    await this.authService.attemptAuth(data)
      .then(res => {
        if (res) {
          console.log(res);
          console.log('Logged in! Navigating...');
          this.router.navigate(['/']);
        } else {
          console.log('Log in failed!');
        }
        this.waiting.next(false);
      });
  }
}
