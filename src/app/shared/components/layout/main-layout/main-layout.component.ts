import {Component, Input, OnInit} from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ThemingService } from '../../../../theming/theming.service';
import { IUser } from '../../../../models/data';
import { Observable } from 'rxjs';
import {AuthService} from '../../../../auth/services';

@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html'
})
export class MainLayoutComponent implements OnInit {
    currentUser: Observable<IUser>;

    public config: PerfectScrollbarConfigInterface;
    public currentTheme: string;

    constructor(
      private readonly theming: ThemingService,
      private readonly authService: AuthService,
    ) {
      this.currentUser = this.authService.currentUser;
    }

    ngOnInit() {
        // Set the default theming at start
        this.theming.theming$.subscribe(
            (newTheme => {
                this.currentTheme = newTheme;
            })
        );
    }
}
