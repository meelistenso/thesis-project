import { Component, Input, EventEmitter } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

import { AppSettings } from '../../../../settings';
import {IUser} from '../../../../models/data';
import {Observable} from 'rxjs';
import {AuthService} from '../../../../auth/services';
import {Router} from '@angular/router';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html'
})
export class ToolbarComponent {
    @Input() currentUser: Observable<IUser>;

    public hasBreadcrumb: boolean;
    private toggleMenu: boolean;

    @Input() showLanguage: boolean;
    @Input() showAccount: boolean;
    @Input() showLogout: boolean;
    @Input() toggleMenu$: EventEmitter<boolean> | boolean;
    @Input() sidenav: MatSidenav;

    constructor(
        private readonly authService: AuthService,
        private readonly router: Router
    ) {
        this.hasBreadcrumb = AppSettings.hasBreadcrumb;
    }

    onToggleMenu(): void {
        this.toggleMenu = !this.toggleMenu;
        this.sidenav.toggle();

        if (this.toggleMenu$ instanceof EventEmitter) {
            this.toggleMenu$.emit(this.toggleMenu);
        }
    }

    async logout(): Promise<void> {
      await this.authService.logout();
      this.authService.purgeAuth();
      await this.router.navigate(['/login']);
    }
}
