import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { DialogLogoutComponent } from '../../../dialogs';

@Component({
    selector: 'app-logout-button',
    templateUrl: './logout-button.component.html'
})
export class LogoutButtonComponent {
    @Output() onLogoutPressed = new EventEmitter();

    constructor(private dialog: MatDialog) { }

    public openLogoutDialog(): void {
        // Open logout dialog
        const logoutDialog = this.dialog.open(DialogLogoutComponent, {
            width: '300px'
        });

        logoutDialog.afterClosed().subscribe(result => {
            if (result) {
                console.log('Logout pressed');
                this.onLogoutPressed.emit();
            }
        });

    }
}
