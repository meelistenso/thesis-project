import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AuthService } from '../../../../auth/services';
import { ValidationService, ValidationType, ValidatorType } from '../../../services/validation.service';
import { RegisterDto } from '../../../../api/models/dto/auth';
import { BehaviorSubject, from, ReplaySubject } from 'rxjs';
import { AddTestDto } from '../../../../api/models/dto/test';
import { TopicTestApiService } from '../../../../api/services/data';
import { TestService} from '../../../../test/services/test.service';

@Component({
  selector: 'app-create-test-form',
  templateUrl: './create-test-form.component.html',
  styleUrls: ['./create-test-form.component.scss']
})
export class CreateTestFormComponent {
  @Input() waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  @Output() onSubmit = new EventEmitter<AddTestDto>();

  title: string = '';
  titleError: string = '';

  description: string = '';
  descriptionError: string = '';

  topicTestId: number = -1;
  topicTestIdError: string = '';
  topicTestIdOptions = new BehaviorSubject<any>([]);

  constructor(
    private readonly authService: AuthService,
    private readonly validationService: ValidationService,
    private readonly testService: TestService
  ) {  }

  handleTitleChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [],
      ValidationType.OnInput,
    );
    this.titleError = validationErrors[0];
    this.title = value;
  }

  handleDescriptionChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [],
      ValidationType.OnInput,
    );
    this.descriptionError = validationErrors[0];
    this.description = value;
  }

  handleTopicTestIdChange(option: any) {
    const validationErrors = this.validationService.validate(
      option.value.toString(),
      [],
      ValidationType.OnInput,
    );
    this.topicTestIdError = validationErrors[0];
    this.topicTestId = option.value;
  }

  async handleTopicTestIdSearchChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [],
      ValidationType.OnInput,
    );
    this.topicTestIdError = validationErrors[0];
    if (value) {
      const options = await this.testService.searchTopics(value);
      this.topicTestIdOptions.next(
        options.List
          .map(option => ({
              label: option.Name,
              value: option.Id,
            }),
          ),
      );
    } else {
      this.topicTestIdOptions.next([]);
    }
  }

  async submit() {
    const titleErrors = this.validationService.validate(
      this.title,
      [ValidatorType.Required],
      ValidationType.OnSubmit,
    );

    const descriptionErrors = this.validationService.validate(
      this.description,
      [ValidatorType.Required],
      ValidationType.OnSubmit,
    );

    const topicTestIdErrors = this.validationService.validate(
      this.topicTestId.toString(),
      [ValidatorType.Required, ValidatorType.Id],
      ValidationType.OnSubmit,
    );

    this.titleError = titleErrors[0];
    this.descriptionError = descriptionErrors[0];
    this.topicTestIdError = topicTestIdErrors[0];

    const hasErrors = titleErrors.length
      || descriptionErrors.length
      || topicTestIdErrors.length;

    if (!hasErrors) {
      const data: AddTestDto = {
        Test: {
          Title: this.title,
          Description: this.description,
          TopicTestId: this.topicTestId
        }
      };

      this.waiting.next(true);

      this.onSubmit.emit(data);
    }
  }
}
