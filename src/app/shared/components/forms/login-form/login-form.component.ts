import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import { AuthService } from '../../../../auth/services';
import { ValidationService, ValidationType, ValidatorType } from '../../../services/validation.service';
import { LoginDto } from '../../../../api/models/dto/auth';
import { LocalStorageService } from '../../../../local-storage/local-storage.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Input() waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  @Output() onSubmit = new EventEmitter<LoginDto>();

  username: string = '';
  usernameError: string = '';
  password: string = '';
  passwordError: string = '';

  private _rememberMe: BehaviorSubject<boolean>;

  get rememberMe() {
    return this._rememberMe.asObservable();
  }

  constructor(
    private readonly authService: AuthService,
    private readonly validationService: ValidationService,
    private readonly localStorageService: LocalStorageService,
  ) { }

  ngOnInit() {
    const rememberMe = this.localStorageService.getValue<boolean>('rememberMe', 'auth');
    this._rememberMe = new BehaviorSubject<boolean>(rememberMe);

    if (rememberMe) {
      const loginData = this.localStorageService.getValue<LoginDto>('loginData', 'auth');

      if (loginData) {
        this.username = loginData.Username;
        this.password = loginData.Password;
      } else {
        this._rememberMe.next(false);
      }
    }
  }

  setRememberMe(rememberMe: boolean) {
    this.localStorageService.setValue<boolean>(rememberMe, 'rememberMe', 'auth');
    if (!rememberMe) {
      this.localStorageService.setValue<LoginDto>(undefined, 'loginData', 'auth');
    }
    this._rememberMe.next(rememberMe);
  }

  handleUsernameChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [ValidatorType.Email],
      ValidationType.OnInput,
    );
    this.usernameError = validationErrors[0];
    this.username = value;
  }

  handlePasswordChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [ValidatorType.Password],
      ValidationType.OnInput,
    );
    this.passwordError = validationErrors[0];
    this.password = value;
  }

  async submit() {
    const usernameValidationErrors = this.validationService.validate(
      this.username,
      [ValidatorType.Required, ValidatorType.Email],
      ValidationType.OnSubmit,
    );

    const passwordValidationErrors = this.validationService.validate(
      this.password,
      [ValidatorType.Required],
      ValidationType.OnSubmit,
    );

    this.usernameError = usernameValidationErrors[0];
    this.passwordError = passwordValidationErrors[0];

    const hasErrors =
      usernameValidationErrors.length ||
      passwordValidationErrors.length;

    if (!hasErrors) {
      const data: LoginDto = { Username: this.username, Password: this.password };

      this.waiting.next(true);

      if (this._rememberMe.getValue()) {
        this.localStorageService.setValue<LoginDto>(data, 'loginData', 'auth');
      }

      this.onSubmit.emit(data);
    }
  }
}
