import {Component, EventEmitter, Input, Output} from '@angular/core';
import { AuthService } from '../../../../auth/services';
import { ValidationService, ValidationType, ValidatorType } from '../../../services/validation.service';
import { RegisterDto } from '../../../../api/models/dto/auth';
import {ReplaySubject} from 'rxjs';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent {
  @Input() waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  @Output() onSubmit = new EventEmitter<RegisterDto>();

  firstName: string = '';
  firstNameError: string = '';
  lastName: string = '';
  lastNameError: string = '';
  email: string = '';
  emailError: string = '';
  password: string = '';
  passwordError: string = '';

  constructor(
    private readonly authService: AuthService,
    private readonly validationService: ValidationService,
  ) {  }

  handleFirstNameChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [ValidatorType.Name],
      ValidationType.OnInput,
    );
    this.firstNameError = validationErrors[0];
    this.firstName = value;
  }

  handleLastNameChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [ValidatorType.Name],
      ValidationType.OnInput,
    );
    this.lastNameError = validationErrors[0];
    this.lastName = value;
  }

  handleEmailChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [ValidatorType.Email],
      ValidationType.OnInput,
    );
    this.emailError = validationErrors[0];
    this.email = value;
  }

  handlePasswordChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [ValidatorType.Password],
      ValidationType.OnInput,
    );
    this.passwordError = validationErrors[0];
    this.password = value;
  }

  async submit() {
    const firstNameValidationErrors = this.validationService.validate(
      this.firstName,
      [ValidatorType.Required, ValidatorType.Name],
      ValidationType.OnSubmit,
    );

    const lastNameValidationErrors = this.validationService.validate(
      this.lastName,
      [ValidatorType.Required, ValidatorType.Name],
      ValidationType.OnSubmit,
    );

    const emailValidationErrors = this.validationService.validate(
      this.email,
      [ValidatorType.Required, ValidatorType.Email],
      ValidationType.OnSubmit,
    );

    const passwordValidationErrors = this.validationService.validate(
      this.password,
      [ValidatorType.Required],
      ValidationType.OnSubmit,
    );

    this.firstNameError = firstNameValidationErrors[0];
    this.lastNameError = lastNameValidationErrors[0];
    this.emailError = emailValidationErrors[0];
    this.passwordError = passwordValidationErrors[0];

    const hasErrors = firstNameValidationErrors.length
      || lastNameValidationErrors.length
      || emailValidationErrors.length
      || passwordValidationErrors.length;

    if (!hasErrors) {
      const data: RegisterDto = {
        Email: this.email,
        FirstName: this.firstName,
        LastName: this.lastName,
        Password: this.password
      };

      this.waiting.next(true);

      this.onSubmit.emit(data);
    }
  }
}
