import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AuthService } from '../../../../auth/services';
import { ValidationService, ValidationType, ValidatorType } from '../../../services/validation.service';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { EditTestDto} from '../../../../api/models/dto/test';
import { TestService} from '../../../../test/services/test.service';

@Component({
  selector: 'app-edit-test-form',
  templateUrl: './edit-test-form.component.html',
  styleUrls: ['./edit-test-form.component.scss']
})
export class EditTestFormComponent {
  @Input() waiting: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  @Output() onSubmit = new EventEmitter<EditTestDto>();

  @Input() title: string = '';
  titleError: string = '';

  @Input() description: string = '';
  descriptionError: string = '';

  @Input() topicTestId: number = -1;
  topicTestIdError: string = '';
  topicTestIdOptions = new BehaviorSubject<any>([]);

  constructor(
    private readonly authService: AuthService,
    private readonly validationService: ValidationService,
    private readonly testService: TestService
  ) {  }

  handleTitleChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [],
      ValidationType.OnInput,
    );
    this.titleError = validationErrors[0];
    this.title = value;
  }

  handleDescriptionChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [],
      ValidationType.OnInput,
    );
    this.descriptionError = validationErrors[0];
    this.description = value;
  }

  handleTopicTestIdChange(option: any) {
    const validationErrors = this.validationService.validate(
      option.value.toString(),
      [],
      ValidationType.OnInput,
    );
    this.topicTestIdError = validationErrors[0];
    this.topicTestId = option.value;
  }

  async handleTopicTestIdSearchChange(value: string) {
    const validationErrors = this.validationService.validate(
      value,
      [],
      ValidationType.OnInput,
    );
    this.topicTestIdError = validationErrors[0];
    if (value) {
      const options = await this.testService.searchTopics(value);
      this.topicTestIdOptions.next(
        options.List
          .map(option => ({
              label: option.Name,
              value: option.Id,
            }),
          ),
      );
    } else {
      this.topicTestIdOptions.next([]);
    }
  }

  async submit() {
    const titleErrors = this.validationService.validate(
      this.title,
      [ValidatorType.Required],
      ValidationType.OnSubmit,
    );

    const descriptionErrors = this.validationService.validate(
      this.description,
      [ValidatorType.Required],
      ValidationType.OnSubmit,
    );

    const topicTestIdErrors = this.validationService.validate(
      this.topicTestId.toString(),
      [ValidatorType.Required, ValidatorType.Id],
      ValidationType.OnSubmit,
    );

    this.titleError = titleErrors[0];
    this.descriptionError = descriptionErrors[0];
    this.topicTestIdError = topicTestIdErrors[0];

    const hasErrors = titleErrors.length
      || descriptionErrors.length
      || topicTestIdErrors.length;

    if (!hasErrors) {
      const data: EditTestDto = {
          Title: this.title,
          Description: this.description,
          TopicTestId: this.topicTestId
      };

      this.waiting.next(true);

      this.onSubmit.emit(data);
    }
  }
}
