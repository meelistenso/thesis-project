import { Component, OnInit, Input, Inject, PLATFORM_ID, EventEmitter, Output } from '@angular/core';

import { isPlatformBrowser } from '@angular/common';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

enum SelectSizes {
  Primary = 'primary',
  Secondary = 'secondary',
}

enum SelectStyles {
  Primary = 'primary',
  Secondary = 'secondary',
  Tertiary = 'tertiary',
}

interface Option {
  label: string;
  value: any;
}

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent {
  _options = new BehaviorSubject<Option[]>([]);

  @Input() set options(options: Option[]) {
    console.log(options);
    this._options.next(options);
  }

  @Input() error: string = '';
  @Input() icon: string = '';
  @Input('value')
  set value(value: string) {
    this.inputFormControl.setValue(value);
  }
  @Input() type: string = '';
  @Input() name: string = '';
  @Input() disabled: boolean = false;
  @Input() placeholder: string = '';
  @Input() componentSize: SelectSizes = SelectSizes.Primary;
  @Input() componentStyle: SelectStyles = SelectStyles.Primary;
  @Output() onValueChange = new EventEmitter<string>();
  @Output() onSelect = new EventEmitter<Option>();

  /** Error when invalid control is dirty, touched, or submitted. */
  matcher = new class implements ErrorStateMatcher {
    constructor(public superThis: SelectComponent) {}
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = form && form.submitted;
      return !!(control && this.superThis.error && (control.dirty || control.touched || isSubmitted));
    }
  }(this);

  inputFormControl = new FormControl('');

  isBrowser: boolean;

  searchTimeout: any;

  constructor(@Inject(PLATFORM_ID) platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  get componentSizeClass(): string {
    switch (this.componentSize) {
      case SelectSizes.Primary:
        return 'Select--size-primary';
      case SelectSizes.Secondary:
        return 'Select--size-secondary';
      default:
        return '';
    }
  }

  get componentStyleClass(): string {
    switch (this.componentStyle) {
      case SelectStyles.Primary:
        return 'Select--style-primary';
      case SelectStyles.Secondary:
        return 'Select--style-secondary';
      case SelectStyles.Tertiary:
        return 'Select--style-tertiary';
      default:
        return '';
    }
  }

  handleValueChange(event) {
    const value: string = event.target.value;
    clearTimeout(this.searchTimeout);
    this.searchTimeout = setTimeout(() => {
      this.onValueChange.emit(value);
    }, 200);
  }

  select(option) {
    this.onSelect.emit(option);
  }

}
