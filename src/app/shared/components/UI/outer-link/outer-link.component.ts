﻿import {Component, OnInit, Input, Inject, PLATFORM_ID} from '@angular/core';

import { isPlatformBrowser} from '@angular/common';

enum LinkStyles {
    Primary = 'primary',
    Accent = 'accent',
    Trasnperent = 'transperent',
}

@Component({
    selector: 'app-outer-link',
    templateUrl: './outer-link.component.html',
    styleUrls: ['./outer-link.component.scss']
})
export class OuterLinkComponent {
    @Input() componentStyle: LinkStyles = LinkStyles.Primary;
    @Input() isBlank: boolean = false;
    @Input() href: string = '';

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case LinkStyles.Primary:
                return 'Link--primary';
            case LinkStyles.Trasnperent:
                return 'Link--transperent';
            case LinkStyles.Accent:
                return 'Link--accent';
            default:
                return 'Link--primary';
        }
    }

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }
}
