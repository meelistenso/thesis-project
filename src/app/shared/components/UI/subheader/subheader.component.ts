import { Component, Input } from '@angular/core';

interface IDot {
    link: string;
    text: string;
}

@Component({
    selector: 'app-subheader',
    templateUrl: './subheader.component.html',
    styleUrls: ['./subheader.component.scss']
})
export class SubheaderComponent {
    @Input() dots: IDot[] = [];
}
