﻿import { Component, OnInit, Input, Inject, PLATFORM_ID, EventEmitter, Output } from '@angular/core';

import { isPlatformBrowser } from '@angular/common';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';

enum InputSizes {
    Primary = 'primary',
    Secondary = 'secondary',
}

enum InputStyles {
    Primary = 'primary',
    Secondary = 'secondary',
    Tertiary = 'tertiary',
}

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class InputComponent {
    @Input() error: string = '';
    @Input() icon: string = '';
    @Input('value')
    set value(value: string) {
      this.inputFormControl.setValue(value);
    }
    @Input() type: string = '';
    @Input() name: string = '';
    @Input() disabled: boolean = false;
    @Input() placeholder: string = '';
    @Input() componentSize: InputSizes = InputSizes.Primary;
    @Input() componentStyle: InputStyles = InputStyles.Primary;
    @Output() onValueChange = new EventEmitter<string>();

    /** Error when invalid control is dirty, touched, or submitted. */
    matcher = new class implements ErrorStateMatcher {
      constructor(public superThis: InputComponent) {}
      isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && this.superThis.error && (control.dirty || control.touched || isSubmitted));
      }
    }(this);

    inputFormControl = new FormControl('');

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    get componentSizeClass(): string {
        switch (this.componentSize) {
            case InputSizes.Primary:
                return 'Input--size-primary';
            case InputSizes.Secondary:
                return 'Input--size-secondary';
        default:
            return '';
        }
    }

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case InputStyles.Primary:
                return 'Input--style-primary';
            case InputStyles.Secondary:
                return 'Input--style-secondary';
            case InputStyles.Tertiary:
                return 'Input--style-tertiary';
        default:
            return '';
        }
    }

    handleValueChange(event) {
        const value: string = event.target.value;
        this.onValueChange.emit(value);
    }
}
