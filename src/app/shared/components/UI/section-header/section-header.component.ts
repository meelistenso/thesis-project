﻿import { Component, OnInit, Input, Inject } from '@angular/core';

enum SectionHeaderStyles {
    Movies = 'movies',
    Plan = 'plan',
    Account = 'account',
}

@Component({
    selector: 'app-section-header',
    templateUrl: './section-header.component.html',
    styleUrls: ['./section-header.component.scss']
})
export class SectionHeaderComponent {
    @Input() componentStyle: SectionHeaderStyles = SectionHeaderStyles.Movies;
    @Input() text: string = '';

    get sectionImage() {
        return `url(${this.sectionImageLink})`;
    }

    private get sectionImageLink(): string {
        switch (this.componentStyle) {
            case SectionHeaderStyles.Movies:
                return '/img/HeaderImgMovies.png';
            case SectionHeaderStyles.Plan:
                return '/img/HeaderImgMovies.png';
            case SectionHeaderStyles.Account:
                return '/img/HeaderImgAccount.png';
        }
    }
}
