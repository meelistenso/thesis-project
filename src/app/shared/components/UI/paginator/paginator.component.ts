﻿import { Component, OnInit, Input, Inject, PLATFORM_ID, Output, EventEmitter } from '@angular/core';
import { isPlatformBrowser} from '@angular/common';

enum PaginatorStyles {
    Primary = 'primary',
}

const PAGES_TO_SHOW: number = 2;

interface IPaginatorButton {
    index: number;
    isShown: boolean;
}

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent {
    @Input() componentStyle: PaginatorStyles = PaginatorStyles.Primary;
    @Input() currentPage: number = 0;
    @Input() pagesCount: number = 0;

    @Output() onPageChange = new EventEmitter();

    get showLeftJump(): boolean { return this.currentPage - PAGES_TO_SHOW > 0; }
    get showLeftArrow(): boolean { return this.currentPage > 0; }
    get showRighJump(): boolean { return this.pagesCount - this.currentPage - PAGES_TO_SHOW > 1; }
    get showRightArrow(): boolean { return this.pagesCount - this.currentPage - 1 > 0; }
    get showButtons(): boolean { return this.pagesCount > 1; }

    get totalPages(): number { return Math.ceil(this.pagesCount / PAGES_TO_SHOW); };
    get totalPagesArray(): IPaginatorButton[] {
        const array = new Array(this.pagesCount).fill(null)
        const buttonArray = array.map((v, index) => {
            const value = {
                index,
                isShown: this.isIndexShown(index),
            }
            return value;
        })
        return buttonArray;
    };

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case PaginatorStyles.Primary:
                return 'Paginator--primary';
            default:
                return '';
        }
    }

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    public handleLeftClick() {
        this.onPageChange.emit(this.currentPage - 1);
    };
    public handleRightClick() {
        this.onPageChange.emit(this.currentPage + 1);
    };
    public handleLeftJump() {
        this.onPageChange.emit(0);
    };
    public handleRightJump() {
        this.onPageChange.emit(this.pagesCount - 1);
    };
    public handlePageClick(index: number) {
        this.onPageChange.emit(index);
        console.log('onpageeclick', index)
    };

    private isIndexShown(i: number) {
        const currentInBounds = Math.abs(this.currentPage - i) <= PAGES_TO_SHOW;
        const currentSmall = this.currentPage <= PAGES_TO_SHOW && i <= PAGES_TO_SHOW * 2;
        const currentBig = this.currentPage >= this.pagesCount - PAGES_TO_SHOW && i + 1 >= this.pagesCount - PAGES_TO_SHOW * 2;
        const showIndex = currentInBounds || currentSmall || currentBig;

        return showIndex;
    }
}
