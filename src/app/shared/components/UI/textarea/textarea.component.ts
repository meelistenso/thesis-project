﻿import {Component, OnInit, Input, Inject, PLATFORM_ID, EventEmitter, Output} from '@angular/core';

import { isPlatformBrowser } from '@angular/common';

enum TextareaSizes {
    Primary = 'primary',
    Secondary = 'secondary',
}
enum TextareaStyles {
    Primary = 'primary',
    Secondary = 'secondary',
}

@Component({
    selector: 'app-textarea',
    templateUrl: './textarea.component.html',
    styleUrls: ['./textarea.component.scss']
})
export class TextareaComponent {
    @Input() error: string = '';
    @Input() value: string = '';
    @Input() disabled: boolean = false;
    @Input() placeholder: string = '';
    @Input() rows: string = '5';
    @Input() componentSize: TextareaSizes = TextareaSizes.Primary;
    @Input() componentStyle: TextareaStyles = TextareaStyles.Primary;
    @Output() onValueChange = new EventEmitter<string>();

    isBrowser: boolean;
    
    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    get componentSizeClass(): string {
        switch (this.componentSize) {
            case TextareaSizes.Primary:
                return 'Textarea--size-primary';
            case TextareaSizes.Secondary:
                return 'Textarea--size-secondary';
        default:
            return '';
        }
    }

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case TextareaStyles.Primary:
                return 'Textarea--style-primary';
            case TextareaStyles.Secondary:
                return 'Textarea--style-secondary';
        default:
            return '';
        }
    }

    handleValueChange(event) {
        const value: string = event.target.value;
        this.onValueChange.emit(value);
    }
}
