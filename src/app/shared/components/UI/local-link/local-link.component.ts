﻿import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-local-link',
    templateUrl: './local-link.component.html',
    styleUrls: ['./local-link.component.scss']
})
export class LocalLinkComponent {
    @Input() componentStyle: string = '';
    @Input() href: string = '';
    @Input() query: any = {};

    get reference(): string {
        return `/${this.href}`;
    }
}
