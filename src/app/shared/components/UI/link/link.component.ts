﻿import {Component, OnInit, Input, Inject, PLATFORM_ID} from '@angular/core';

import { isPlatformBrowser} from '@angular/common';

enum LinkStyles {
    Primary = 'primary',
    Breadcrump = 'breadcrump',
    Secondary = 'secondary',
    Trasnperent = 'transperent',
}

@Component({
  selector: 'app-link',
    templateUrl: './link.component.html',
    styleUrls: ['./link.component.scss']
})
export class LinkComponent {
    @Input() componentStyle: LinkStyles = LinkStyles.Primary;
    @Input() href: string = '';
    @Input() query: any = {};

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case LinkStyles.Primary:
                return 'Link--primary';
            case LinkStyles.Breadcrump:
                return 'Link--breadcrump';
            case LinkStyles.Secondary:
                return 'Link--secondary';
            case LinkStyles.Trasnperent:
                return 'Link--transperent';
            default:
                return '';
        }
    }

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }
}
