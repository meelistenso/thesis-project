﻿import {Component, OnInit, Input, Inject, PLATFORM_ID, EventEmitter, Output} from '@angular/core';

import { isPlatformBrowser } from '@angular/common';

enum InputSizes {
    Primary = 'primary'
}

enum InputStyles {
    Primary = 'primary'
}

@Component({
    selector: 'app-checkbox',
    templateUrl: './checkbox.component.html',
    styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {
    @Input() error: string = '';
    @Input() checked: boolean = false;
    @Input() label: string = '';
    @Input() disabled: boolean = false;
    @Input() componentSize: InputSizes = InputSizes.Primary;
    @Input() componentStyle: InputStyles = InputStyles.Primary;
    @Output() onValueChange = new EventEmitter<boolean>();

    labelPosition: 'before' | 'after' = 'after';

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    get componentSizeClass(): string {
        switch (this.componentSize) {
            case InputSizes.Primary:
                return 'Checkbox--size-primary';
        default:
            return '';
        }
    }

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case InputStyles.Primary:
                return 'Checkbox--style-primary';
        default:
            return '';
        }
    }

    toggleCheck() {
        const value: boolean = !this.checked;
        if (!this.disabled) {
            this.onValueChange.emit(value);
        }
    }
}
