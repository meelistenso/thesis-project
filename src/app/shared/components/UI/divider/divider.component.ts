﻿import { Component, OnInit, Input, Inject, PLATFORM_ID } from '@angular/core';

import { isPlatformBrowser } from '@angular/common';

enum DividerStyles {
    Default = 'default'
}

@Component({
    selector: 'app-divider',
    templateUrl: './divider.component.html',
    styleUrls: ['./divider.component.scss']
})
export class DividerComponent {
    @Input() componentStyle: DividerStyles = DividerStyles.Default;

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case DividerStyles.Default:
                return 'Divider--default';
            default:
                return '';
        }
    }

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }
}
