import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { CdkStepper } from '@angular/cdk/stepper';
import {CheckQuestionDto} from '../../../../api/models/dto/test';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: StepperComponent }]
})
export class StepperComponent extends CdkStepper {

  @Input('selectedIndex')
  set setSelectedIndex(index: number) {
    if (this.steps && this.steps.length > index) {
      this.selectedIndex = index;
    }
  }

  get isNextDisabled() {
    return !this.allowNext || !this.allowNext.includes(this.selectedIndex);
  }

  @Input() showComplete = false;
  @Input() allowNext: number[];
  @Output() completeClick = new EventEmitter();
  @Output() selectClick = new EventEmitter<number>();

  onClick(index: number): void {
    if (this.selectedIndex + 1 === this.steps.length) {
      this.complete();
    } else {
      this.selectClick.emit(index);
    }
  }

  complete() {
    this.completeClick.emit();
  }

}
