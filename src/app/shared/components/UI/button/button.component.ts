﻿import {Component, OnInit, Input, Inject, PLATFORM_ID, EventEmitter, Output} from '@angular/core';

import { isPlatformBrowser } from '@angular/common';

enum ButtonStyles {
    Primary = 'primary',
    Secondary = 'secondary',
    Transparent = 'transparent',
    Inverted = 'inverted',
}

enum ButtonSizes {
    Primary = 'primary',
    Secondary = 'secondary',
}

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
    @Input() componentStyle: ButtonStyles = ButtonStyles.Primary;
    @Input() componentSize: ButtonSizes = ButtonSizes.Primary;
    @Input() disabled: boolean = false;
    @Input() type: string = 'button';
    @Output() onButtonClick = new EventEmitter();

    get componentStyleClass(): string {
        switch (this.componentStyle) {
            case ButtonStyles.Primary:
                return 'Button--style-primary';
            case ButtonStyles.Secondary:
                return 'Button--style-secondary';
            case ButtonStyles.Transparent:
                return 'Button--style-transparent';
            case ButtonStyles.Inverted:
                return 'Button--style-inverted';
            default:
                return '';
        }
    }

    get componentSizeClass(): string {
        switch (this.componentSize) {
            case ButtonSizes.Primary:
                return 'Button--size-primary';
            case ButtonSizes.Secondary:
                return 'Button--size-secondary';
            default:
                return '';
        }
    }

    isBrowser: boolean;

    constructor(@Inject(PLATFORM_ID) platformId: string) {
        this.isBrowser = isPlatformBrowser(platformId);
    }

    handleButtonClick() {
        this.onButtonClick.emit();
    }
}
