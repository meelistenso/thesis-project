﻿import { Injectable } from '@angular/core';

export enum ValidatorType {
    Email,
    Name,
    Comment,
    Phone,
    Password,
    Required,
    PassportNumber,
    Address,
    CompanyName,
    Id,
}

export enum ValidationType {
    OnSubmit,
    OnInput,
}

interface IValidator {
    [key: string]: (value: string) => string[];
}

type Validators = {
    [key: string]: IValidator;
};

export const selectValidator = (
    validatorType: ValidatorType,
    validationType: ValidationType,
): ((value: string) => string[]) | undefined => {
    switch (validatorType) {
        case ValidatorType.Email:
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.Email.OnSubmit;
                case ValidationType.OnInput:
                    return validators.Email.OnInput;
                default:
                    return;
            }
        case ValidatorType.Name:
            switch (validationType) {
            case ValidationType.OnSubmit:
                return validators.Name.OnSubmit;
            case ValidationType.OnInput:
                return validators.Name.OnInput;
            default:
                return;
            }
        case ValidatorType.Phone:
            switch (validationType) {
            case ValidationType.OnSubmit:
                return validators.Phone.OnSubmit;
            case ValidationType.OnInput:
                return validators.Phone.OnInput;
            default:
                return;
            }
        case ValidatorType.Comment:
            switch (validationType) {
            case ValidationType.OnSubmit:
                return validators.Comment.OnSubmit;
            case ValidationType.OnInput:
                return validators.Comment.OnInput;
            default:
                return;
            }
        case ValidatorType.Password:
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.Password.OnSubmit;
                case ValidationType.OnInput:
                    return validators.Password.OnInput;
                default:
                    return;
            }
        case ValidatorType.Required:
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.Required.OnSubmit;
                case ValidationType.OnInput:
                    return validators.Required.OnInput;
                default:
                    return;
            }
        case ValidatorType.Address:
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.Address.OnSubmit;
                case ValidationType.OnInput:
                    return validators.Address.OnInput;
                default:
                    return;
            }
        case ValidatorType.PassportNumber:
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.PassportNumber.OnSubmit;
                case ValidationType.OnInput:
                    return validators.PassportNumber.OnInput;
                default:
                    return;
            }
        case ValidatorType.CompanyName:
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.CompanyName.OnSubmit;
                case ValidationType.OnInput:
                    return validators.CompanyName.OnInput;
                default:
                    return;
            }
        case ValidatorType.Id: {
            switch (validationType) {
                case ValidationType.OnSubmit:
                    return validators.Id.OnSubmit;
                case ValidationType.OnInput:
                    return validators.Id.OnInput;
                default:
                    return;
            }
        }
        default:
            return;
    }
};

export const validators: Validators = {
    Email: {
        OnSubmit: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g,
            );

            if (!validationPassed) {
                errors.push('Unsupported format');
            }

            return errors;
        },
        OnInput: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(/^[a-zA-Z0-9@_.+-]*$/);
            if (!validationPassed) {
                errors.push('Unsupported character');
            }

            return errors;
        },
    },
    Password: {
        OnSubmit: (value: string): string[] => {
            const errors: string[] = [];

            const minWidthPassed = value.length >= 8;
            if (!minWidthPassed) {
                errors.push('Password is too short');
            }

            const maxWidthPassed = value.length <= 30;
            if (!maxWidthPassed) {
                errors.push('Password is too long');
            }

            const validationPassed = value.match(
                /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+\\/'‘’?:.(){}[\]~\-_])[0-9a-zA-Z!@#$%^&+\\/'‘’?:.,(){}[\]~\-_]{8,}$/,
            );
            if (!validationPassed) {
                const hasSpecialCharacter = value.match(
                    /[!@#$%^&+\\/'‘’?:.,(){}[\]~\-_]+/,
                );
                const hasNumber = value.match(/[0-9]+/);
                const hasUppercaseLetter = value.match(/[A-Z]+/);
                const hasLowercaseLetter = value.match(/[a-z]+/);
                if (!hasSpecialCharacter) {
                    errors.push('Password must contain at least one special character');
                } else if (!hasNumber) {
                    errors.push('Password must contain at least one number');
                } else if (!hasLowercaseLetter) {
                    errors.push('Password must contain at least one lowercase letter');
                } else if (!hasUppercaseLetter) {
                    errors.push('Password must contain at least one uppercase letter');
                } else {
                    errors.push('Invalid password');
                }
            }

            return errors;
        },
        OnInput: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(
                /^[A-Za-z\d!@#$%^&+\\/'‘’?:.,(){}[\]~\-_\*]*$/,
            );
            if (!validationPassed) {
                errors.push('Unsupported character');
            }

            return errors;
        },
    },
    Name: {
        OnSubmit: (value: string): string[] => {
            const errors: string[] = [];

            const minWidthPassed = value.length >= 2;
            if (!minWidthPassed) {
                errors.push('Name is too short');
            }

            const maxWidthPassed = value.length <= 30;
            if (!maxWidthPassed) {
                errors.push('Name is too long');
            }

            const validationPassed = value.match(/^[a-zA-Z.-]*$/);
            if (!validationPassed) {
                errors.push('Unsupported character');
            }

            return errors;
        },
        OnInput: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(/^[a-zA-Z.-]*$/);
            if (!validationPassed) {
                errors.push('Unsupported character');
            }

            return errors;
        },
    },
    Comment: {
        OnSubmit: (value: string): string[] => {
            const errors: string[] = [];

            const minWidthPassed = value.length >= 2;
            if (!minWidthPassed) {
                errors.push('Comment is too short');
            }

            const maxWidthPassed = value.length <= 1000;
            if (!maxWidthPassed) {
                errors.push('Comment is too long');
            }

            return errors;
        },
        OnInput: (value: string): string[] => {
            return [];
        },
    },
    Phone: {
        OnSubmit: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/);
            if (!validationPassed) {
                errors.push('Unsupported format');
            }

            return errors;
        },
        OnInput: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(/^[0-9+]*$/);
            if (!validationPassed) {
                errors.push('Unsupported character');
            }

            return errors;
        },
    },
    Required: {
        OnSubmit: (value: string): string[] => {
            const errors: string[] = [];

            const validationPassed = value.match(/^(?!\s*$).+/);
            if (!validationPassed) {
                errors.push('Field shouldn\'t be empty');
            }

            return errors;
        },
        OnInput: (value: string): string[] => {
            return [];
        },
    },
    Id: {
      OnSubmit: (value: string): string[] => {
          const errors: string[] = [];

          const validationPassed = value.match( /^[1-9]\d*$/g );

          if (!validationPassed) {
            errors.push('Field shouldn\'t be empty');
          }

          return errors;
      },
      OnInput: (value: string): string[] => {
        return [];
      },
    },
    Address: {
        OnSubmit: (value: string): string[] => {
            return [];
        },
        OnInput: (value: string): string[] => {
            return [];
        },
    },
    PassportNumber: {
        OnSubmit: (value: string): string[] => {
            return [];
        },
        OnInput: (value: string): string[] => {
            return [];
        },
    },
    CompanyName: {
        OnSubmit: (value: string): string[] => {
            return [];
        },
        OnInput: (value: string): string[] => {
            return [];
        },
    },
};

@Injectable()
export class ValidationService {
    public static validatorType = ValidatorType;
    public static validationType = ValidationType;

    public validate(
        value: string,
        validatorTypes: ValidatorType[],
        validationType: ValidationType,
    ): string[] {
        return validatorTypes
            .reduce(
                (accumulator, currentValue) => {
                    const validate = selectValidator(currentValue, validationType);
                    if (validate) {
                        const validationErrors = validate(value);
                        return [...accumulator, ...validationErrors];
                    }
                    return accumulator;
                },
                [] as string[],
            )
            .filter(validationError => !!validationError);
    };
}
