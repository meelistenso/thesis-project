import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {ITestData} from '../../../models/data';
import {MatPaginator} from '@angular/material/paginator';
import {TestService} from '../../services/test.service';
import {ActivatedRoute, Router} from '@angular/router';
import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import {from} from 'rxjs';
import {TopicListComponent} from '..';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements AfterViewInit {

  displayedColumns: string[] = ['Id', 'Title', 'Description'];
  data: ITestData[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly testService: TestService,
    private readonly router: Router,
    private readonly route: ActivatedRoute
  ) {}

  ngAfterViewInit() {
    this.route.params.subscribe(params => {
      const topicId = params['id'];
      console.log(params);
      this.paginator.page
        .pipe(
          startWith({}),
          switchMap(() => {
            this.isLoadingResults = true;
            return this.testService.getTestsByTopic(topicId, this.paginator.pageIndex);
          }),
          map(data => {
            // Flip flag to show that loading has finished.
            this.isLoadingResults = false;
            this.isRateLimitReached = false;

            this.resultsLength = data.Count;
            return data.List;
          }),
          catchError(() => {
            this.isLoadingResults = false;
            // Catch if the API has reached its rate limit. Return empty data.
            this.isRateLimitReached = true;
            return from([]);
          })
        ).subscribe(data => this.data = data);
    });
  }

  async select(data: ITestData) {
    await this.router.navigate(['/tests', data.Id]);
  }

}
