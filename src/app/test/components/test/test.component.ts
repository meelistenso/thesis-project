import { Component, OnInit } from '@angular/core';
import { TestControllerService } from '../../services/test-controller.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { CheckQuestionDto } from '../../../api/models/dto/test';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {
  isLinear = true;

  questions = this.testControllerService.questions;
  questionsCount = this.testControllerService.questionsCount;
  currentQuestion = this.testControllerService.currentQuestion;
  testData = this.testControllerService.testData;

  get allowNext(): Observable<number[]> {
    return this.testControllerService.allowNext;
  }

  get showComplete(): boolean {
    return this.testControllerService.isCompleted;
  }

  constructor(
    private readonly testControllerService: TestControllerService,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const id = params['id'];
      console.log('testId:', id, params);
      if (id != null) {
        this.testControllerService.initTest(id);
      }
    });
  }

  nextQuestion(i: number) {
    this.testControllerService.nextQuestion(i);
  }

  async complete() {
    const results = await this.testControllerService.checkAnswers();
    this.testControllerService.setResults(results);
    await this.router.navigate(['tests', 'result']);
  }

}
