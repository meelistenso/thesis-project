import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionShortComponent } from './question-short.component';

describe('QuestionShortComponent', () => {
  let component: QuestionShortComponent;
  let fixture: ComponentFixture<QuestionShortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionShortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionShortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
