import {Component, Input, OnInit} from '@angular/core';
import {IQuestion} from '../../../../models/data';
import {TestControllerService} from '../../../services/test-controller.service';

@Component({
  selector: 'app-question-short',
  templateUrl: './question-short.component.html',
  styleUrls: ['./question-short.component.scss']
})
export class QuestionShortComponent implements OnInit {

  @Input() question: IQuestion;

  selected: number;

  constructor(private readonly testControllerService: TestControllerService) {}

  ngOnInit(): void {
  }

  select(i: number) {
    this.selected = i;
    this.testControllerService.addAnswer({
        QuestionId: this.question.Id,
        Answer: this.question.Versions[this.selected].Title
      }
    );
    console.log(this.selected);
  }

}
