import {Component, Input, OnInit} from '@angular/core';
import {IQuestion} from '../../../../models/data';
import {TestControllerService} from '../../../services/test-controller.service';

@Component({
  selector: 'app-question-multiselect',
  templateUrl: './question-multiselect.component.html',
  styleUrls: ['./question-multiselect.component.scss']
})
export class QuestionMultiselectComponent implements OnInit {

  @Input() question: IQuestion;

  selected: number;

  constructor(private readonly testControllerService: TestControllerService) {}

  ngOnInit(): void {
  }

  select(i: number) {
    this.selected = i;
    this.testControllerService.addAnswer({
        QuestionId: this.question.Id,
        Answer: this.question.Versions[this.selected].Title
      }
    );
    console.log(this.selected);
  }

}
