import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCompilanceComponent } from './question-compilance.component';

describe('QuestionCompilanceComponent', () => {
  let component: QuestionCompilanceComponent;
  let fixture: ComponentFixture<QuestionCompilanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionCompilanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionCompilanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
