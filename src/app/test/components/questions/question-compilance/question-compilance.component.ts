import {Component, Input, OnInit} from '@angular/core';
import {IQuestion} from '../../../../models/data';
import {TestControllerService} from '../../../services/test-controller.service';

@Component({
  selector: 'app-question-compilance',
  templateUrl: './question-compilance.component.html',
  styleUrls: ['./question-compilance.component.scss']
})
export class QuestionCompilanceComponent implements OnInit {

  @Input() question: IQuestion;

  selected: number;

  constructor(private readonly testControllerService: TestControllerService) {}

  ngOnInit(): void {
  }

  select(i: number) {
    this.selected = i;
    this.testControllerService.addAnswer({
        QuestionId: this.question.Id,
        Answer: this.question.Versions[this.selected].Title
      }
    );
    console.log(this.selected);
  }

}
