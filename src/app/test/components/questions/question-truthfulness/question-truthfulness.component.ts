import {Component, Input, OnInit} from '@angular/core';
import {IQuestion} from '../../../../models/data';
import {TestControllerService} from '../../../services/test-controller.service';

@Component({
  selector: 'app-question-truthfulness',
  templateUrl: './question-truthfulness.component.html',
  styleUrls: ['./question-truthfulness.component.scss']
})
export class QuestionTruthfulnessComponent implements OnInit {

  @Input() question: IQuestion;

  selected: number;

  constructor(private readonly testControllerService: TestControllerService) {}

  ngOnInit(): void {
  }

  select(i: number) {
    this.selected = i;
    this.testControllerService.addAnswer({
        QuestionId: this.question.Id,
        Answer: this.question.Versions[this.selected].Title
      }
    );
    console.log(this.selected);
  }

}
