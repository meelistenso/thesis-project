import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionTruthfulnessComponent } from './question-truthfulness.component';

describe('QuestionTruthfulnessComponent', () => {
  let component: QuestionTruthfulnessComponent;
  let fixture: ComponentFixture<QuestionTruthfulnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionTruthfulnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionTruthfulnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
