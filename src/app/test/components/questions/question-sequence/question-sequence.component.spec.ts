import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionSequenceComponent } from './question-sequence.component';

describe('QuestionSequenceComponent', () => {
  let component: QuestionSequenceComponent;
  let fixture: ComponentFixture<QuestionSequenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionSequenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionSequenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
