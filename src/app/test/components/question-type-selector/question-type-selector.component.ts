import { Component, Input, OnInit } from '@angular/core';
import { IQuestion } from '../../../models/data';

import { QuestionType } from '../../../models/data/question-type.enum';

@Component({
  selector: 'app-question-type-selector',
  templateUrl: './question-type-selector.component.html',
  styleUrls: ['./question-type-selector.component.scss']
})
export class QuestionTypeSelectorComponent implements OnInit {
  QuestionType = QuestionType;
  @Input() question: IQuestion;

  // get type() {
  //   if (this.question) {
  //     return this.question.type;
  //   }
  // }

  constructor() {
  }

  ngOnInit(): void {
  }

}
