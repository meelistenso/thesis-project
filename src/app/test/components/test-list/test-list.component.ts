import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { TestService } from '../../services/test.service';
import { from } from 'rxjs';
import {ITestData} from '../../../models/data';
import {Router} from '@angular/router';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.scss']
})
export class TestListComponent implements AfterViewInit {

  displayedColumns: string[] = ['Id', 'Title', 'Description'];
  data: ITestData[] = [];

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private readonly testService: TestService,
    private readonly router: Router
  ) {}

  ngAfterViewInit() {
    this.paginator.page
      .pipe(
        startWith({}),
        switchMap(() => {
          this.isLoadingResults = true;
          return this.testService.getPopularTests(this.paginator.pageIndex);
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          this.isLoadingResults = false;
          this.isRateLimitReached = false;

          this.resultsLength = data.Count;
          return data.List;
        }),
        catchError(() => {
          this.isLoadingResults = false;
          // Catch if the API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return from([]);
        })
      ).subscribe(data => this.data = data);
  }

  async select(data: ITestData) {
    await this.router.navigate(['/tests', data.Id]);
  }
}
