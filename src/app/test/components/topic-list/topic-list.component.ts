import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Router } from '@angular/router';
import { TestService } from '../../services/test.service';
import { GetTestTopicRo } from '../../../api/models/ro/topic-test';

@Component({
  selector: 'app-topic-list',
  templateUrl: './topic-list.component.html',
  styleUrls: ['./topic-list.component.scss']
})
export class TopicListComponent implements OnInit {
  treeControl = new NestedTreeControl<GetTestTopicRo>(node => node.TopicTestModels);
  dataSource = new MatTreeNestedDataSource<GetTestTopicRo>();

  constructor(
    private readonly router: Router,
    private readonly testService: TestService
  ) { }

  async ngOnInit() {
    const data = await this.testService.getTopicTree();
    this.dataSource.data = data.List;
  }

  async openNode(node: GetTestTopicRo) {
    await this.router.navigate(['tests', 'topic-list', node.Id]);
  }

  hasChild = (_: number, node: GetTestTopicRo) => !!node.TopicTestModels && node.TopicTestModels.length > 0;
}
