import { Component, OnInit } from '@angular/core';
import { TestControllerService } from '../../services/test-controller.service';
import {map} from 'rxjs/operators';
import {combineLatest} from 'rxjs';

@Component({
  selector: 'app-test-result',
  templateUrl: './test-result.component.html',
  styleUrls: ['./test-result.component.scss']
})
export class TestResultComponent implements OnInit {

  constructor(private readonly testControllerService: TestControllerService) { }

  results = this.testControllerService.results;
  mark = this.results.pipe(map(results => results.Mark));
  answers = this.results.pipe(
    map(results => results.Answers),
  );
  questions = this.testControllerService.questions;

  answerEntries = combineLatest(this.answers, this.questions)
    .pipe(
      map(entry => {
        const ans = entry[0];
        const ques = entry[1];
        return ans.map(a => {
          const q = ques.find(value => value.Id = a.QuestionId);
          return { ...a, title: q.Title };
        });
      })
    );

  ngOnInit(): void {
  }

}
