import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestService } from './services/test.service';
import { ApiModule } from '../api/api.module';
import { SharedModule } from '../shared/shared.module';

import { TestListComponent } from './components/test-list/test-list.component';
import { TestComponent } from './components/test/test.component';
import { QuestionTypeSelectorComponent } from './components/question-type-selector/question-type-selector.component';
import { QuestionComponent } from './components/question/question.component';
import { TestStepComponent } from './components/test-step/test-step.component';
import { QuestionTruthfulnessComponent } from './components/questions/question-truthfulness/question-truthfulness.component';
import { QuestionSelectComponent } from './components/questions/question-select/question-select.component';
import { QuestionMultiselectComponent } from './components/questions/question-multiselect/question-multiselect.component';
import { QuestionShortComponent } from './components/questions/question-short/question-short.component';
import { QuestionSequenceComponent } from './components/questions/question-sequence/question-sequence.component';
import { QuestionNumberComponent } from './components/questions/question-number/question-number.component';
import { QuestionCompilanceComponent } from './components/questions/question-compilance/question-compilance.component';
import { ReferenceComponent } from './components/reference/reference.component';
import { TestRoutingModule } from './test-routing.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatStepperModule} from '@angular/material/stepper';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {TestControllerService} from './services/test-controller.service';
import { TopicListComponent } from './components/topic-list/topic-list.component';
import { TopicComponent } from './components/topic/topic.component';
import { TestResultComponent } from './components/test-result/test-result.component';
import {MatTreeModule} from '@angular/material/tree';

@NgModule({
  declarations: [
    TestListComponent,
    TestComponent,
    QuestionTypeSelectorComponent,
    QuestionComponent,
    TestStepComponent,
    QuestionTruthfulnessComponent,
    QuestionSelectComponent,
    QuestionMultiselectComponent,
    QuestionShortComponent,
    QuestionSequenceComponent,
    QuestionNumberComponent,
    QuestionCompilanceComponent,
    ReferenceComponent,
    TopicListComponent,
    TopicComponent,
    TestResultComponent,
  ],
  providers: [TestService, TestControllerService],
  imports: [
    CommonModule,
    ApiModule,
    SharedModule,
    TestRoutingModule,
    MatProgressSpinnerModule,
    MatStepperModule,
    CdkStepperModule,
    MatTreeModule,
  ]
})
export class TestModule { }
