import { Injectable } from '@angular/core';
import { TestApiService } from '../../api/services/data';
import { BehaviorSubject, combineLatest, from, merge, Observable, ReplaySubject } from 'rxjs';
import { CheckQuestionRo, GetTestRo } from '../../api/models/ro/test';
import { filter, map } from 'rxjs/operators';
import { IQuestion, ITestData } from '../../models/data';
import { CheckQuestionDto } from '../../api/models/dto/test';

@Injectable()
export class TestControllerService {
  private _currentTest = new BehaviorSubject<GetTestRo>(null);
  private _currentQuestion = new ReplaySubject<number>(1);
  private _answers = new BehaviorSubject<CheckQuestionDto[]>([]);

  private _results = new BehaviorSubject<CheckQuestionRo>(null);
  get results(): Observable<CheckQuestionRo> {
    return this._results.asObservable();
  }

  get isCompleted(): boolean {
    if (this._currentTest.getValue() && this._answers.getValue()) {
      return this._currentTest.getValue().Questions.length === this._answers.getValue().length;
    } else {
      return false;
    }
  }

  get allowNext() {
    return combineLatest(this.questions, this.answers)
      .pipe(map(value =>
        value[0].map(question => value[1].findIndex(answer => answer.QuestionId === question.Id))
      ));
  }

  get currentTest(): Observable<GetTestRo> {
    return this._currentTest.asObservable();
  }

  get currentQuestion(): Observable<number> {
    return this._currentQuestion.asObservable();
  }

  get questions(): Observable<IQuestion[]> {
    return this._currentTest.pipe(filter(value => !!value), map(test => test.Questions));
  }

  get answers(): Observable<CheckQuestionDto[]> {
    return this._answers.asObservable();
  }

  get questionsCount(): Observable<number> {
    return this._currentTest.pipe(map(test => test.Questions.length));
  }

  get testData(): Observable<ITestData> {
    return this._currentTest.pipe(map(test =>
      ({
        Id: test.Id,
        Title: test.Title,
        Description: test.Description,
        TopicTestId: test.TopicTestId
      })
    ));
  }

  get question(): Observable<IQuestion> {
    return combineLatest(this._currentQuestion, this._currentQuestion).pipe(
      map(value => {
        const questions = value[0];
        const position = value[1];
        return questions[position];
      })
    );
  }

  constructor(private readonly testApiService: TestApiService) {
  }

  async initTest(id: number) {
    console.log('initTest', id);
    const testData = await this.testApiService.getTestById(id);
    if (testData) {
      this._currentTest.next(testData);
      this._currentQuestion.next(0);
    }
  }

  async checkAnswers(): Promise<CheckQuestionRo> {
    const dto: CheckQuestionDto[] = this._answers.getValue();
    const result = await this.testApiService.checkQuestions(dto, this._currentTest.getValue().Id);
    console.log(result);
    return result;
  }

  nextQuestion(i: number) {
    this._currentQuestion.next(i)
  }

  addAnswer(answer: CheckQuestionDto) {
    const answers = this._answers.getValue();
    const existingAnswer = this._answers.getValue().findIndex(ans => ans.QuestionId === answer.QuestionId);
    if (existingAnswer !== -1) {
      answers[existingAnswer].Answer = answer.Answer;
      this._answers.next(answers);
    } else {
      this._answers.next([...answers, answer]);
    }
  }

  setResults(results: CheckQuestionRo) {
    this._results.next(results);
  }

  resetAnswers() {
    this._answers.next([]);
    this._currentQuestion.next(0);
  }

  reset() {
    this.resetAnswers();
    this._currentTest.next(null);
    this._results.next(null);
  }
}
