import { Injectable } from '@angular/core';
import {TestApiService, TopicTestApiService} from '../../api/services/data';
import {AddTestDto, EditTestDto} from '../../api/models/dto/test';

const ITEMS_PER_PAGE = 10;

@Injectable()
export class TestService {
  constructor(
    private readonly testApiService: TestApiService,
    private readonly topicTestApiService: TopicTestApiService
  ) { }

  getPopularTests(pageIndex: number) {
    return this.testApiService.getPopularTests(pageIndex * ITEMS_PER_PAGE, ITEMS_PER_PAGE);
  }

  getTestsByTopic(topicId: number, pageIndex: number) {
    return this.testApiService.getTestsByTopicId(topicId, pageIndex * ITEMS_PER_PAGE, ITEMS_PER_PAGE);
  }

  getTopicTree() {
    return this.topicTestApiService.getAllTestTopics();
  }

  searchTopics(value: string) {
    return this.topicTestApiService.searchTopics(value);
  }

  addTest(dto: AddTestDto) {
    return this.testApiService.addTest(dto);
  }

  editTest(dto: EditTestDto) {
    return this.testApiService.editTest(dto);
  }

  getTestById(id: number) {
    return this.testApiService.getTestById(id);
  }

}
