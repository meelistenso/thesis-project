import { TestBed } from '@angular/core/testing';

import { TestControllerService } from './test-controller.service';

describe('TestControllerService', () => {
  let service: TestControllerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestControllerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
