import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestListComponent, TestComponent, TestResultComponent, TopicListComponent, TopicComponent} from './components';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'popular',
        pathMatch: 'full',
      },
      {
        path: 'popular',
        component: TestListComponent,
      },
      {
        path: 'topic-list',
        component: TopicListComponent,
      },
      {
        path: 'topic-list/:id',
        component: TopicComponent,
      },
      {
        path: 'result',
        component: TestResultComponent,
      },
      {
        path: ':id',
        component: TestComponent,
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule {}
