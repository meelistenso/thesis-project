import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'testing-system';

  constructor(
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.authService.populate();
  }
}
